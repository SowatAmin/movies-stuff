<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MovieController@index');
Route::post('search','MovieController@search');
Route::get('movie/id/{id}','MovieController@getMovieId');
Route::get('movie_page/{id}','MovieController@showIndividualMovie');
Route::get('popular_movie','MovieController@getPopularMovies');
Route::get('now_playing','MovieController@getNowPlaying');




