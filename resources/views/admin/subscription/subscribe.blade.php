   <!-- subscribe -->
    <div class="row">
    <div class="text-center col-md-12">
      <div class="subscribe-text"><strong> <h3 class="subscribe">Get the best Movie & TV Shows trailers<br>straight in your inbox every week.</h3></strong></div>
      <div class="row">
        <div class="col-md-3 col-md-offset-3"><input type="text" class="form-control" placeholder="First Name"></div>
        <div class="col-md-3"><input type="text" class="form-control" placeholder="Email"></div>
      </div>
      <div class="submit">
          <button type="button" class="btn btn-success btn-m">Subscribe</button>
        </div>
    </div> 
  </div>