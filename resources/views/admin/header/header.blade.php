<nav class="navbar navbar-default">
  <div class="container-fluid"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
      <a class="navbar-brand" href="/">MOVIES&STUFF</a></div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="defaultNavbar1">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="/">Movies<span class="sr-only">(current)</span></a></li>
        <li><a href="#">Tvseries</a></li>
        <li><a href="#">Celebs & Photos</a></li>
        <li><a href="#">News</a></li> 
      </ul>
      
      <form class="navbar-form navbar-left" action="/search" method="POST">
        <div class="form-group">
          <input type="text" class="form-control" name="name" placeholder="Search">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>

    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>