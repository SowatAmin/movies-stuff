@extends('layout')
@section('content')
<!--movie poster-->
<div class="container-fluid">
   <div class="poster-wrapper" style="background-image: url(https://image.tmdb.org/t/p/original/{{ $featured_movie['backdrop_path'] }});">
      <div class="row">
         <div class="poster-box">
            <div class="col-md-offset-1 col-md-4">
               <!-- <div class="embed-responsive embed-responsive-16by9"> -->
               <div class="youtube-player" data-id="{{ $trailer[0]['key'] }}">
                  <!-- trailer -->
               </div>
            </div>
            <div class="col-md-5 description">
               <h3>{{ $featured_movie['title'] }}
                  @for($i = 0; $i <$rate ; $i++)
                  <i class="fa fa-star"></i>
                  @endfor
                  out of 5
               </h3>
               <p>{{ $featured_movie['overview'] }}</p>
               <!-- <p>Genres: Action | Adventure | Comedy | Romance | Sci-Fi</p> -->
               <p><strong>Genres:</strong>
                  @foreach($featured_movie['genres'] as $key => $genres)
                  {{ $genres['name'] }} &nbsp;
                  @endforeach
               </p>
               <p><strong>Runtime:</strong> {{ $featured_movie['runtime'] }}mins. </p>
               <p><strong>Rating:</strong> {{ $featured_movie['vote_average'] }} </p>
               <p><strong>Release Date:</strong> {{ $featured_movie['release_date'] }}. </p>
               <p><strong>Revenue:</strong> ${{ $featured_movie['revenue'] }}. </p>
            </div>
         </div>
      </div>
   </div>
</div>
<!--movie poster ends-->
<div class="container trailer">
<h2 style="text-align: center;">Official Trailers</h2>
@for($i = 1; $i<count($trailer); $i++)
<div class="row">
   <div class="col-md-6">
      <div class="youtube-player" data-id="{{ $trailer[$i]['key'] }}"> </div>
   </div>
   @endfor
</div>
<div class="container content">
   <!-- popular movie -->
   <div class="movie-wrapper">
      <div class="row">
         <div class="col-md-6">
            <h4>Popular Movies</h4>
         </div>
         <div class="col-md-2 col-md-offset-4">
            <a href="/popular_movie">
               <h5>View All Movies</h5>
            </a>
         </div>
      </div>
      <div class="row">
         @for($i=0;$i<=5;$i++)
         <a href="/movie_page/{{ $popular_movies[$i]['id'] }}">
            <div class="col-md-2">
               <img src="http://image.tmdb.org/t/p/w185/{{ $popular_movies[$i]['poster_path'] }}" class="img-responsive" alt="Placeholder image">
               <h4>{{ $popular_movies[$i]['title'] }} </h4>
         </a>
         </div>
         @endfor
      </div>
   </div>
   <!--  -->
   <hr>
   <div class="tvseries-wrapper">
      <div class="row">
         <div class="col-md-6">
            <h4>Now Playing</h4>
         </div>
         <div class="col-md-2 col-md-offset-4">
            <a href="/now_playing">
               <h5>View All Movies</h5>
            </a>
         </div>
      </div>
      <div class="row">
         @for($i=0;$i<=5;$i++)
         <a href="/movie_page/{{ $latest_movies[$i]['id'] }}">
            <div class="col-md-2">
               <img src="http://image.tmdb.org/t/p/w185/{{ $latest_movies[$i]['poster_path'] }}" class="img-responsive" alt="Placeholder image">
               <h4>{{ $latest_movies[$i]['title'] }} </h4>
         </a>
         </div>
         @endfor  
      </div>
   </div>
   <hr>
</div>
@endsection()

@section('essentialscripts')
<script>
  
      document.addEventListener("DOMContentLoaded",
        function() {
            var div, n,
                v = document.getElementsByClassName("youtube-player");
            for (n = 0; n < v.length; n++) {
                div = document.createElement("div");
                div.setAttribute("data-id", v[n].dataset.id);
                div.innerHTML = labnolThumb(v[n].dataset.id);
                div.onclick = labnolIframe;
                v[n].appendChild(div);
            }
        });

    function labnolThumb(id) {
        var thumb = '<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',
            play = '<div class="play"></div>';
        return thumb.replace("ID", id) + play;
    }

    function labnolIframe() {
        var iframe = document.createElement("iframe");
        var embed = "https://www.youtube.com/embed/ID?autoplay=1";
        iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "1");
        this.parentNode.replaceChild(iframe, this);
    }
</script>
@endsection()