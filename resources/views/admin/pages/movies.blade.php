@extends('layout')
@section('content')

<div class="container">
   <h2 style="text-align: center;">{{ $page_title }}</h2>
   @foreach($movies as $movie)
   <div class="well">
      <div class="row">
         <div class="col-md-3"><img src="http://image.tmdb.org/t/p/w185/{{ $movie['poster_path'] }}" class="img-responsive" alt="Placeholder image">
         </div>
         <div class="col-md-6">
            <div class="movie_descriptions">
               <h3>
                  <a href="movie_page/{{ $movie['id'] }}">
                     <h4>{{ $movie['title'] }}</h4>
                  </a>
               </h3>
               <p>Description:{{ $movie['overview'] }} </p>
               <p>Rating:{{ $movie['vote_average'] }}</p>
               <p>
                  <button type="button" class="btn btn-primary btn-lg">
                     <a href="movie_page/{{ $movie['id'] }}">
               <h4 style="color:white;">Details</h4></a></button></p>
            </div>
         </div>
      </div>
   </div>
   @endforeach
</div>

@endsection()

@section('essentialscripts')
@endsection()