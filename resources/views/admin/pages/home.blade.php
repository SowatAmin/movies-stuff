@extends('layout')
@section('content')
<!--movie poster-->
<div class="container-fluid">
   <div class="poster-wrapper" style="background-image: url(https://image.tmdb.org/t/p/original/{{ $featured_movie['backdrop_path'] }});">
      <div class="row">
         <div class="poster-box">
            <div class="col-md-offset-1 col-md-4">
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="video-box" width="560" height="500" src="https://www.youtube.com/embed/{{ $trailer }}" frameborder="1" allow="autoplay; encrypted-media" allowfullscreen></iframe>
               </div>
            </div>
            <div class="col-md-5 description">
               <a href="/movie_page/{{ $featured_movie['id'] }}" style="color: white;">
                  <h3>{{ $featured_movie['title'] }}
               </a>
               </h3>
               <h3>
                  @for($i = 0; $i <$rate ; $i++)
                  <i class="fa fa-star"></i>
                  @endfor
                  out of 5
               </h3>
               <p>{{ $featured_movie['overview'] }}</p>
               <p><strong>Rating:</strong> {{ $featured_movie['vote_average'] }} </p>
               <p><strong>Release Date:</strong> {{ $featured_movie['release_date'] }}. </p>
            </div>
         </div>
      </div>
   </div>
</div>
<!--movie poster ends-->
<div class="container content">
   <!-- popular movie -->
   <div class="movie-wrapper">
      <div class="row">
         <div class="col-md-6">
            <h4>Popular Movies</h4>
         </div>
         <div class="col-md-2 col-md-offset-4">
            <a href="/popular_movie">
               <h5>View All Movies</h5>
            </a>
         </div>
      </div>
      <div class="row">
         @for($i=0;$i<=5;$i++)
         <a href="/movie_page/{{ $popular_movies[$i]['id'] }}">
            <div class="col-md-2">
               <img src="http://image.tmdb.org/t/p/w185/{{ $popular_movies[$i]['poster_path'] }}" class="img-responsive" alt="Placeholder image">
               <h4>{{ $popular_movies[$i]['title'] }} </h4>
         </a>
         </div>
         @endfor
      </div>
   </div>
   <!--  -->
   <hr>
   <div class="tvseries-wrapper">
      <div class="row">
         <div class="col-md-6">
            <h4>Now Playing</h4>
         </div>
         <div class="col-md-2 col-md-offset-4">
            <a href="/now_playing">
               <h5>View All Movies</h5>
            </a>
         </div>
      </div>
      <div class="row">
         @for($i=0;$i<=5;$i++)
         <a href="/movie_page/{{ $latest_movies[$i]['id'] }}">
            <div class="col-md-2">
               <img src="http://image.tmdb.org/t/p/w185/{{ $latest_movies[$i]['poster_path'] }}" class="img-responsive" alt="Placeholder image">
               <h4>{{ $latest_movies[$i]['title'] }} </h4>
         </a>
         </div>
         @endfor  
      </div>
   </div>
   <hr>
</div>
@endsection()

@section('essentialscripts')
@endsection()