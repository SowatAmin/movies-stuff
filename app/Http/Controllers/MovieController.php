<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tmdb\Laravel\Facades\Tmdb;

class MovieController extends Controller
{
    
    public function index()
    {
        $popular_movie = Tmdb::getMoviesApi()->getPopular();
        $popular_movies = array_slice($popular_movie['results'], 0, 6, true);
        
        $get_latest = Tmdb::getMoviesApi()->getNowPlaying();
        
        $latest_movies = array_slice($get_latest['results'], 0, 6, true);
        $featured_movie = $latest_movies[rand(0,5)];
       
        $rate = floor($featured_movie['vote_average'])/2; 
        $featured_movie_id = $featured_movie['id'];
        
        /*fetching trailer*/
          $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.themoviedb.org/3/movie/".$featured_movie_id."/videos?language=en-US&api_key=bc37e55902a828eecf9d12f6dc8beec0",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
          ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);

        }
       
         $trailer = $response['results']['0']['key'];


        return view('admin.pages.home',compact('popular_movies','latest_movies','featured_movie','trailer','rate'));
    }

   
    public function search(Request $request)
    {
        $data=$request->name;
        $data=str_replace(" ","%20",$data);
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.themoviedb.org/3/search/movie?include_adult=false&page=1&query=".$data."&language=en-US&api_key=bc37e55902a828eecf9d12f6dc8beec0",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "{}",
          ));

        $response = curl_exec($curl);        
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
           $response = json_decode($response, true);
           $movies=$response['results'];
           $page_title = "Search Results";
           return view('admin.pages.movies',compact('movies','page_title'));
          }
    }

    public function showIndividualMovie($id)
    {
        $popular_movie = Tmdb::getMoviesApi()->getPopular();
        $popular_movies = array_slice($popular_movie['results'], 0, 6, true);
        $get_latest = Tmdb::getMoviesApi()->getNowPlaying();
        $latest_movies = array_slice($get_latest['results'], 0, 6, true);
        $featured_movie = Tmdb::getMoviesApi()->getMovie($id);
        $rate = floor($featured_movie['vote_average'])/2;
        
        /*fetching movie trailer*/
        $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.themoviedb.org/3/movie/".$id."/videos?language=en-US&api_key=bc37e55902a828eecf9d12f6dc8beec0",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
          ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            //$trailer = $response['results']['0']['key'];
            if(empty($response['results'])){
              $trailer = 0;
          }
            else{
              $trailer = $response['results'];  
            }            
        }
        return view('admin.pages.individual_movie',compact('popular_movies','latest_movies','featured_movie','trailer','rate'));
    }

   
    public function getPopularMovies()
    {
      $movies = Tmdb::getMoviesApi()->getPopular();
      $movies = $movies['results'];
      $page_title = "Popular Movies";
      return view('admin.pages.movies',compact('movies','page_title'));      
    }
    public function getNowPlaying()
    {
      $movies = Tmdb::getMoviesApi()->getNowPlaying();
      $movies = $movies['results'];
      $page_title = "Now Playing";
      return view('admin.pages.movies',compact('movies','page_title'));       
    }    
}
